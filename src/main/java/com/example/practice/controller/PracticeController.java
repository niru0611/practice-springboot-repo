package com.example.practice.controller;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.practice.entity.Student;
import com.example.practice.service.PracticeService;


@RestController
@RequestMapping("api/students")
public class PracticeController {
	
	@Autowired
	PracticeService practiceService;
	
	@PostMapping(value="/")
	public Student addStudent(@RequestBody Student student) {
		return practiceService.addStudent(student);
	}
	
	@PutMapping(value="/")
	public Student updateStudent(@RequestBody Student student) {
		return practiceService.updateStudent(student);
	}
	
	@GetMapping(value="/")
	public List<Student> getallStudents() {
		return practiceService.getallStudents();
	}
	
	@GetMapping(value="/{id}")
	public Student getStudentbyID(@PathVariable("id") int id) {
		return practiceService.getStudentbyID(id);
	}
	
	@DeleteMapping(value="/{id}")
	public void deleteStudent(@PathVariable("id") int id) {
		practiceService.deleteStudent(id);
	}
	


}
