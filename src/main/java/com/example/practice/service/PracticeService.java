package com.example.practice.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.practice.entity.Student;
import com.example.practice.repository.PracticeRepository;

@Service
public class PracticeService {
	
	@Autowired
	private PracticeRepository practiceRepository;
	
	public Student addStudent(Student student) {
		return practiceRepository.addStudent(student);
	}
	
	public Student updateStudent(Student student) {
		return practiceRepository.updateStudent(student);
	}
	
	public List<Student> getallStudents() {
		return practiceRepository.getAllStudents();
	}
	

	public Student getStudentbyID(int id) {
		return practiceRepository.getStudentbyID(id);
	}
	public void deleteStudent(int id) {
		practiceRepository.deleteStudent(id);
	}
	


}
