package com.example.practice.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.practice.entity.Student;

@Repository
public class MySQLPracticeRepository implements PracticeRepository{
	
	@Autowired
	private JdbcTemplate template;

	@Override
	public Student addStudent(Student student) {
		String sql = "INSERT INTO studentTable(id,name,dob,bloodGroup) VALUES(?,?,?,?)";
		// TODO Auto-generated method stub
		template.update(sql, student.getId(),student.getName(),student.getDob(),student.getBloodgrp());
		return student;
	}

	@Override
	public Student updateStudent(Student student) {
		String sql = "INSERT INTO studentTable(name,dob,bloodGroup) VALUES(?,?,?) WHERE(id=?)";
		// TODO Auto-generated method stub
		template.update(sql,student.getName(),student.getDob(),student.getBloodgrp(),student.getId());
		return student;
	}

	@Override
	public List<Student> getAllStudents() {
		String sql = "SELECT * FROM studentTable";
		// TODO Auto-generated method stub
		return template.query(sql,new StudentRowMapper());
	}

	@Override
	public Student getStudentbyID(int id) {
		String sql = "SELECT * FROM studentTable WHERE id=?";
		// TODO Auto-generated method stub
		return template.queryForObject(sql,new StudentRowMapper(),id);
	}

	@Override
	public void deleteStudent(int id) {
		String sql="DELETE FROM studentTable WHERE id=?";
		// TODO Auto-generated method stub
		template.update(sql,id);
		
	}

}

class StudentRowMapper implements RowMapper<Student> {

	@Override
	public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return new Student(rs.getInt("id"),rs.getString("name"),rs.getString("dob"),rs.getString("bloodgrp"));
	}
	
}
