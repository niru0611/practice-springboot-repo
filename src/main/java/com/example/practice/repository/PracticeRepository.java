package com.example.practice.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.practice.entity.Student;

@Component
public interface PracticeRepository {
	public Student addStudent(Student student);
	public Student updateStudent(Student student);
	public List<Student> getAllStudents();
	public Student getStudentbyID(int id);
	public void deleteStudent(int id);

}
 