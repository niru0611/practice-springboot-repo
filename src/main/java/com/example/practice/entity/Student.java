package com.example.practice.entity;



public class Student {
	private int id;
	private String name;
	private String dob;
	private String bloodgrp;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getBloodgrp() {
		return bloodgrp;
	}
	public void setBloodgrp(String bloodgrp) {
		this.bloodgrp = bloodgrp;
	}
	public Student(int id, String name, String dob, String bloodgrp) {
		super();
		this.id = id;
		this.name = name;
		this.dob = dob;
		this.bloodgrp = bloodgrp;
	}
	

}
